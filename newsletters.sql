-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 09 déc. 2019 à 15:26
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `newsletters`
--

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `prenom` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `clients_subscribed_to_newsletters`
--

DROP TABLE IF EXISTS `clients_subscribed_to_newsletters`;
CREATE TABLE IF NOT EXISTS `clients_subscribed_to_newsletters` (
  `clients_id` int(11) NOT NULL,
  `newsletters_id` int(11) NOT NULL,
  PRIMARY KEY (`clients_id`,`newsletters_id`),
  KEY `fk_clients_has_newsletters_newsletters1_idx` (`newsletters_id`),
  KEY `fk_clients_has_newsletters_clients_idx` (`clients_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `newsletters`
--

DROP TABLE IF EXISTS `newsletters`;
CREATE TABLE IF NOT EXISTS `newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_news` varchar(255) NOT NULL,
  `desc_news` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `newsletters`
--

INSERT INTO `newsletters` (`id`, `nom_news`, `desc_news`) VALUES
(1, 'offres', 'En vous inscrivant à cette newsletter, vous recevrez directement nos offres par e-mail!'),
(2, 'informations', 'En vous inscrivant à cette newsletter, vous recevrez directement des informations sur nos services par e-mail!');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `clients_subscribed_to_newsletters`
--
ALTER TABLE `clients_subscribed_to_newsletters`
  ADD CONSTRAINT `fk_clients_has_newsletters_clients` FOREIGN KEY (`clients_id`) REFERENCES `clients` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_clients_has_newsletters_newsletters1` FOREIGN KEY (`newsletters_id`) REFERENCES `newsletters` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
