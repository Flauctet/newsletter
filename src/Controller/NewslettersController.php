<?php

namespace App\Controller;

use App\Form\NewsType;
use App\Entity\Clients;
use App\Form\ClientsType;
use App\Entity\Newsletters;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class NewslettersController extends AbstractController
{

    /**
     * @Route("/newsletters/{nom_news}", name="subTo")
     */
    public function subscribeTo($nom_news, Request $request, MailerInterface $mailer)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $newsletter = $entityManager->getRepository(Newsletters::class)->findOneBy(['nomNews' => $nom_news]);

        $client = new Clients();

        $form = $this->createFormBuilder($client)
            ->add('email', EmailType::class)
            ->add('prenom', TextType::class)
            ->add('save', SubmitType::class)
            ->getForm();

        $form = $this->createForm(ClientsType::class, $client);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $client = $form->getData();

            //Si le client existe déjà dans la base de données, nous allons mettre à jour ses informations (prénom si ajouté)
            $ifClientExist = $entityManager->getRepository(Clients::class)->findOneBy(['email' => $client->getEmail()]);
            if ($ifClientExist !== null) {
                $prenom = $client->getPrenom();
                $client = $ifClientExist;
                if ($prenom !== null) {
                    $client->setPrenom($prenom);
                }
            }

            $entityManager->persist($client);
            $entityManager->flush();

            //Si l'utilisateur est déjà abonné, nous l'informons
            if (in_array($client, $newsletter->getClients()->toArray())) return $this->redirectToRoute('issubscribed', ['nom_news' => $nom_news]);


            $newsletter->addClient($client);
            $entityManager->persist($newsletter);
            $entityManager->flush();

            //On envoie le mail de confirmation d'inscription à la newsletter
            $message = (new TemplatedEmail())
                ->from('test-newsletter@flauctet.com')
                ->to($client->getEmail())
                ->subject("Votre inscription à la newsletter $nom_news a bien été prise en compte.")
                ->htmlTemplate('newsletters/mail/ok-mail.html.twig')
                ->context([
                    'nom_news' => $nom_news
                ]);
            $mailer->send($message);


            return $this->redirectToRoute('subscribed', ['nom_news' => $nom_news]);
        }

        return $this->render('newsletters/subscribe.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/subscribed/{nom_news}", name="subscribed")
     */
    public function subscribed($nom_news)
    {
        return $this->render(
            'newsletters/ok.html.twig',
            array("nom_news" => $nom_news)
        );
    }

    /**
     * @Route("/issubscribed/{nom_news}", name="issubscribed")
     */
    public function alreadySubscribed($nom_news)
    {
        return $this->render(
            'newsletters/issub.html.twig',
            array("nom_news" => $nom_news)
        );
    }

    /**
     * @Route("/send_news/", name="send_news")
     */
    public function send_news(Request $request, MailerInterface $mailer)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->add('news', ChoiceType::class)
            ->add('content_news', TextareaType::class)
            ->add('send', SubmitType::class, ['label' => 'Envoyer'])
            ->getForm();

        $form = $this->createForm(NewsType::class, null, ['entityManager' => $entityManager]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $newsletter = $entityManager->getRepository(Newsletters::class)->findOneBy(['id' => $data['news']]);
            $clients = $newsletter->getClients()->toArray();

            foreach ($clients as $client) {
                //On envoie le mail de la newsletter
                $message = (new TemplatedEmail())
                    ->from('test-newsletter@flauctet.com')
                    ->to($client->getEmail())
                    ->subject("Newsletter : " . $newsletter->getNomNews())
                    ->htmlTemplate('newsletters/mail/send-news.html.twig')
                    ->context([
                        'news' => $newsletter,
                        'content_news' => $data['content_news'],
                        'prenom' => $client->getPrenom(),
                        'mail' => $client->getEmail()
                    ]);
                $mailer->send($message);
            }
            return $this->redirectToRoute('send_confirmed');
        }

        return $this->render('newsletters/mail/news.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/send_news/confirmed/", name="send_confirmed")
     */
    public function send_confirmed()
    {
        return $this->render('newsletters/mail/confirmed.html.twig');
    }

    /**
     * @Route("/unsubscribe/{email}/{id_news}", name="unsubscribe")
     */
    public function unsub($email, $id_news)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $client = $entityManager->getRepository(Clients::class)->findOneBy(['email' => $email]);
        $news = $entityManager->getRepository(Newsletters::class)->findOneBy(['id' => $id_news]);
        $client->removeNewsletter($news);
        $entityManager->persist($client);
        $entityManager->flush();
        return $this->render("newsletters/mail/unsub.html.twig");
    }

    /**
     * La modification des abonnements n'est pas fonctionnelle
     */
}
