<?php

namespace App\Controller;

use App\Entity\Newsletters;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $newsletters = $entityManager->getRepository(Newsletters::class);
        return $this->render('pages/index.html.twig', [
            'newsletters' => $newsletters->findAll(),
        ]);
    }
}
