<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Newsletters
 *
 * @ORM\Table(name="newsletters")
 * @ORM\Entity
 */
class Newsletters
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_news", type="string", length=255, nullable=false)
     */
    private $nomNews;

    /**
     * @var string|null
     *
     * @ORM\Column(name="desc_news", type="string", length=500, nullable=true)
     */
    private $descNews;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Clients", mappedBy="newsletters")
     */
    private $clients;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clients = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomNews(): ?string
    {
        return $this->nomNews;
    }

    public function setNomNews(string $nomNews): self
    {
        $this->nomNews = $nomNews;

        return $this;
    }

    public function getDescNews(): ?string
    {
        return $this->descNews;
    }

    public function setDescNews(?string $descNews): self
    {
        $this->descNews = $descNews;

        return $this;
    }

    /**
     * @return Collection|Clients[]
     */
    public function getClients(): Collection
    {
        return $this->clients;
    }

    public function addClient(Clients $client): self
    {
        if (!$this->clients->contains($client)) {
            $this->clients[] = $client;
            $client->addNewsletter($this);
        }

        return $this;
    }

    public function removeClient(Clients $client): self
    {
        if ($this->clients->contains($client)) {
            $this->clients->removeElement($client);
            $client->removeNewsletter($this);
        }

        return $this;
    }

}
