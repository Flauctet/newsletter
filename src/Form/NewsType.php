<?php

namespace App\Form;

use App\Controller\NewslettersController;
use App\Entity\Newsletters;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['entityManager'];
        $news = $entityManager->getRepository(Newsletters::class)->findAll();
        $newsletter = array();
        foreach($news as $n){
            $newsletter[$n->getNomNews()] = $n->getId();
        }
        
        $builder
            ->add('news', ChoiceType::class,[
                'choices' => $newsletter
            ])
            ->add('content_news', TextareaType::class, ['label' => "Contenu de la newsletter", 'required'=>false])
            ->add('send', SubmitType::class, ['label' => 'Envoyer']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);

        $resolver->setRequired('entityManager');
    }
}
